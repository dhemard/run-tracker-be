# Run Tracker

[![pipeline status](https://gitlab.com/dhemard/run-tracker-be/badges/master/pipeline.svg)](https://gitlab.com/dhemard/run-tracker-be/commits/master)
[![coverage report](https://gitlab.com/dhemard/run-tracker-be/badges/master/coverage.svg)](https://gitlab.com/dhemard/run-tracker-be/commits/master)

**Backend** [Frontend](https://gitlab.com/dhemard/run-tracker-fe)

**Analyze your runs and set goals to work toward!**


Built with [Spring Boot](https://projects.spring.io/spring-boot/).




## To Install

- Install Maven if you have not already done so
- Clone the repository
- In a terminal emulator opened in the root directory of the project, run

    ```
    mvn clean install spring-boot:run
    ```
- After the installation is complete and the application is running, you can start the frontend by following the directions listed in its README.md

## Development

[Development Branch](https://gitlab.com/dhemard/run-tracker-be/tree/develop)

[![pipeline status](https://gitlab.com/dhemard/run-tracker-be/badges/develop/pipeline.svg)](https://gitlab.com/dhemard/run-tracker-be/commits/develop)
[![coverage report](https://gitlab.com/dhemard/run-tracker-be/badges/develop/coverage.svg)](https://gitlab.com/dhemard/run-tracker-be/commits/develop)
