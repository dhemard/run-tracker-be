package com.dph.runtracker.managers.impl;

import com.dph.runtracker.accessors.IRunAccessor;
import com.dph.runtracker.accessors.IUserAccessor;
import com.dph.runtracker.converters.IRunConverter;
import com.dph.runtracker.domains.DomainRun;
import com.dph.runtracker.domains.DomainUser;
import com.dph.runtracker.views.ViewRun;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.persistence.EntityNotFoundException;
import java.security.InvalidParameterException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RunManagerTest {

    @Mock
    private IRunAccessor runAccessor;

    @Mock
    private IUserAccessor userAccessor;

    @Mock
    private IRunConverter runConverter;

    @InjectMocks
    private RunManager runManager;

    private DomainUser domainUser;
    private DomainRun domainRun;
    private ViewRun viewRun;

    @Before
    public void setUp() throws Exception {
        domainUser = new DomainUser();
        domainRun = new DomainRun();
        viewRun = new ViewRun();
    }

    @Test
    public void getAllRuns() {
        when(userAccessor.findById(anyInt()))
                .thenReturn(Optional.of(domainUser));
        when(runAccessor.findAllByDomainUser(any(DomainUser.class)))
                .thenReturn(Arrays.asList(domainRun));
        when(runConverter.domainToView(any(DomainRun.class)))
                .thenReturn(viewRun);

        List<ViewRun> results = runManager.getAllRuns(0);
        assertEquals(1, results.size());
        assertEquals(viewRun, results.get(0));
    }

    @Test(expected = EntityNotFoundException.class)
    public void getAllRuns_NoUserWithThatId() {
        when(userAccessor.findById(anyInt()))
                .thenReturn(Optional.empty());

        runManager.getAllRuns(0);
    }

    @Test
    public void getRunById() {
        when(runAccessor.findById(anyInt()))
                .thenReturn(Optional.of(domainRun));
        when(runConverter.domainToView(any(DomainRun.class)))
                .thenReturn(viewRun);

        ViewRun result = runManager.getRunById(0, 0);
        assertEquals(viewRun, result);
    }

    @Test(expected = EntityNotFoundException.class)
    public void getRunById_NoRunWithThatId() {
        when(runAccessor.findById(anyInt()))
                .thenReturn(Optional.empty());

        runManager.getRunById(0, 0);
    }

    @Test
    public void createRun() {
        when(runConverter.viewToDomain(any(ViewRun.class)))
                .thenReturn(domainRun);
        when(runConverter.domainToView(any(DomainRun.class)))
                .thenReturn(viewRun);
        when(runAccessor.save(any(DomainRun.class)))
                .thenReturn(domainRun);

        viewRun.setUserId(0);
        ViewRun result = runManager.createRun(0, viewRun);
        assertEquals(viewRun, result);
    }

    @Test(expected = InvalidParameterException.class)
    public void createRun_NullUserId() {
        runManager.createRun(0, viewRun);
    }

    @Test(expected = InvalidParameterException.class)
    public void createRun_UserIdDoesNotMatchUserIdOfRun() {
        viewRun.setUserId(999);
        runManager.createRun(0, viewRun);
    }

    @Test
    public void updateRun() {
        when(runAccessor.findById(anyInt()))
                .thenReturn(Optional.of(domainRun));
        when(runConverter.viewToDomain(any(ViewRun.class)))
                .thenReturn(domainRun);
        when(runConverter.domainToView(any(DomainRun.class)))
                .thenReturn(viewRun);
        when(runAccessor.save(any(DomainRun.class)))
                .thenReturn(domainRun);

        viewRun.setUserId(0);
        viewRun.setId(0);
        ViewRun result = runManager.updateRun(0, 0, viewRun);
        assertEquals(viewRun, result);
    }

    @Test(expected = InvalidParameterException.class)
    public void updateRun_NullUserId() {
        runManager.updateRun(0, 0, viewRun);
    }

    @Test(expected = InvalidParameterException.class)
    public void updateRun_UserIdDoesNotMatchUserIdOfRun() {
        viewRun.setUserId(999);
        runManager.updateRun(0, 0, viewRun);
    }

    @Test(expected = InvalidParameterException.class)
    public void updateRun_RunIdDoesNotMatchIdOfRun() {
        viewRun.setUserId(0);
        viewRun.setId(999);
        runManager.updateRun(0, 0, viewRun);
    }

    @Test(expected = EntityNotFoundException.class)
    public void updateRun_NoRunWithThatId() {
        when(runAccessor.findById(anyInt()))
                .thenReturn(Optional.empty());
        runManager.deleteRun(0, 0);
    }


    @Test
    public void deleteRun() {
        when(runAccessor.findById(anyInt()))
                .thenReturn(Optional.of(domainRun));
        when(runConverter.domainToView(any(DomainRun.class)))
                .thenReturn(viewRun);
        doNothing().when(runAccessor).deleteById(anyInt());

        domainUser.setId(0);
        domainRun.setDomainUser(domainUser);
        domainRun.setId(0);
        ViewRun result = runManager.deleteRun(0, 0);
        assertEquals(viewRun, result);
    }

    @Test(expected = EntityNotFoundException.class)
    public void deleteRun_NoRunWithThatId() {
        when(runAccessor.findById(anyInt()))
                .thenReturn(Optional.empty());
        runManager.deleteRun(0, 0);
    }

    @Test(expected = InvalidParameterException.class)
    public void deleteRun_UserIdDoesNotMatchUserIdOfRun() {
        when(runAccessor.findById(anyInt()))
                .thenReturn(Optional.of(domainRun));

        domainUser.setId(0);
        domainRun.setDomainUser(domainUser);
        domainRun.setId(0);
        runManager.deleteRun(999, 0);
    }
}