package com.dph.runtracker.managers.impl;

import com.dph.runtracker.accessors.IUserAccessor;
import com.dph.runtracker.converters.IUserConverter;
import com.dph.runtracker.domains.DomainUser;
import com.dph.runtracker.engines.IUserEngine;
import com.dph.runtracker.views.ViewUser;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.persistence.EntityNotFoundException;
import java.security.InvalidParameterException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.Silent.class)
public class UserManagerTest {

    @Mock
    private IUserAccessor userAccessor;

    @Mock
    private IUserConverter userConverter;

    @Mock
    private IUserEngine userEngine;

    @InjectMocks
    private UserManager userManager;

    private DomainUser domainUser;
    private ViewUser viewUser;

    @Before
    public void setUp() {
        domainUser = new DomainUser();
        viewUser = new ViewUser();
    }

    @Test
    public void getAllUsers() {
        when(userAccessor.findAll())
                .thenReturn(Arrays.asList(domainUser));
        when(userConverter.domainToView(any(DomainUser.class)))
                .thenReturn(viewUser);

        List<ViewUser> results = userManager.getAllUsers();
        assertEquals(1, results.size());
        assertEquals(viewUser, results.get(0));
    }

    @Test
    public void getUserById() {
        when(userAccessor.findById(anyInt()))
                .thenReturn(Optional.of(domainUser));
        when(userConverter.domainToView(any(DomainUser.class)))
                .thenReturn(viewUser);

        ViewUser result = userManager.getUserById(1);
        assertEquals(viewUser, result);
    }

    @Test(expected = EntityNotFoundException.class)
    public void getUserById_NoUserWithThatId() {
        when(userAccessor.findById(anyInt()))
                .thenReturn(Optional.empty());

        userManager.getUserById(1);
    }

    @Test
    public void createUser() {
        when(userEngine.usernameIsUnique(anyString()))
                .thenReturn(true);
        when(userConverter.domainToView(any(DomainUser.class)))
                .thenReturn(viewUser);
        when(userConverter.viewToDomain(any(ViewUser.class)))
                .thenReturn(domainUser);
        when(userAccessor.save(any(DomainUser.class)))
                .thenReturn(domainUser);
        viewUser.setUsername("UniqueUsername");

        ViewUser result = userManager.createUser(viewUser);
        assertEquals(viewUser, result);
    }

    @Test(expected = InvalidParameterException.class)
    public void createUser_DuplicateUsername() {
        when(userEngine.usernameIsUnique(anyString()))
                .thenReturn(false);
        viewUser.setUsername("DuplicateUsername");

        userManager.createUser(viewUser);
    }

    @Test
    public void updateUser() {
        when(userEngine.usernameIsUnique(anyString()))
                .thenReturn(true);
        when(userConverter.domainToView(any(DomainUser.class)))
                .thenReturn(viewUser);
        when(userConverter.viewToDomain(any(ViewUser.class)))
                .thenReturn(domainUser);
        when(userAccessor.save(any(DomainUser.class)))
                .thenReturn(domainUser);
        when(userAccessor.findById(anyInt()))
                .thenReturn(Optional.of(domainUser));
        String uniqueUsername = "UNIQUE";
        viewUser.setId(1);
        viewUser.setUsername(uniqueUsername);
        domainUser.setId(1);
        domainUser.setUsername(uniqueUsername);

        ViewUser result = userManager.updateUser(1, viewUser);
        assertEquals(viewUser, result);
    }

    @Test(expected = EntityNotFoundException.class)
    public void updateUser_NoUserWithThatId() {
        when(userAccessor.findById(anyInt()))
                .thenReturn(Optional.empty());
        viewUser.setId(1);
        domainUser.setId(1);

        userManager.updateUser(1, viewUser);
    }

    @Test(expected = InvalidParameterException.class)
    public void updateUser_GivenIdDoesNotMatchGivenUser() {
        when(userAccessor.findById(anyInt()))
                .thenReturn(Optional.of(domainUser));
        viewUser.setId(9999);

        userManager.updateUser(1, viewUser);
    }

    @Test
    public void updateUser_DuplicateUsernameToSameUserSameCase() {
        when(userEngine.usernameIsUnique(anyString()))
                .thenReturn(false);
        when(userConverter.domainToView(any(DomainUser.class)))
                .thenReturn(viewUser);
        when(userConverter.viewToDomain(any(ViewUser.class)))
                .thenReturn(domainUser);
        when(userAccessor.save(any(DomainUser.class)))
                .thenReturn(domainUser);
        when(userAccessor.findById(anyInt()))
                .thenReturn(Optional.of(domainUser));

        String testUsername = "TestUsername";
        domainUser.setId(1);
        domainUser.setUsername(testUsername);
        viewUser.setId(1);
        viewUser.setUsername(testUsername);

        ViewUser result = userManager.updateUser(1, viewUser);
        assertEquals(viewUser, result);
    }

    @Test
    public void updateUser_DuplicateUsernameToSameUserDifferentCase() {
        when(userEngine.usernameIsUnique(anyString()))
                .thenReturn(false);
        when(userConverter.domainToView(any(DomainUser.class)))
                .thenReturn(viewUser);
        when(userConverter.viewToDomain(any(ViewUser.class)))
                .thenReturn(domainUser);
        when(userAccessor.save(any(DomainUser.class)))
                .thenReturn(domainUser);
        when(userAccessor.findById(anyInt()))
                .thenReturn(Optional.of(domainUser));

        String testUsername = "TESTUSERNAME";
        domainUser.setId(1);
        domainUser.setUsername(testUsername.toLowerCase());
        viewUser.setId(1);
        viewUser.setUsername(testUsername);

        ViewUser result = userManager.updateUser(1, viewUser);
        assertEquals(viewUser, result);
    }

    @Test(expected = InvalidParameterException.class)
    public void updateUser_DuplicateUsernameToDifferentUser() {
        when(userEngine.usernameIsUnique(anyString()))
                .thenReturn(false);
        when(userConverter.domainToView(any(DomainUser.class)))
                .thenReturn(viewUser);
        when(userConverter.viewToDomain(any(ViewUser.class)))
                .thenReturn(domainUser);
        when(userAccessor.save(any(DomainUser.class)))
                .thenReturn(domainUser);
        when(userAccessor.findById(anyInt()))
                .thenReturn(Optional.of(domainUser));

        domainUser.setId(1);
        domainUser.setUsername("CurrentUsername");
        viewUser.setId(1);
        viewUser.setUsername("DuplicateDesiredUsername");

        userManager.updateUser(1, viewUser);
    }

    @Test
    public void deleteUser() {
        when(userAccessor.findById(anyInt()))
                .thenReturn(Optional.of(domainUser));
        when(userConverter.domainToView(any(DomainUser.class)))
                .thenReturn(viewUser);
        doNothing().when(userAccessor).deleteById(anyInt());

        ViewUser result = userManager.deleteUser(1);
        assertEquals(viewUser, result);
    }

    @Test(expected = EntityNotFoundException.class)
    public void deleteUser_NoUserWithThatId() {
        when(userAccessor.findById(anyInt()))
                .thenReturn(Optional.empty());

        userManager.deleteUser(9999);
    }
}