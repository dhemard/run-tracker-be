package com.dph.runtracker.managers.impl;

import com.dph.runtracker.accessors.IUserAccessor;
import com.dph.runtracker.converters.IBestConverter;
import com.dph.runtracker.domains.DomainBest;
import com.dph.runtracker.domains.DomainUser;
import com.dph.runtracker.views.ViewBest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.persistence.EntityNotFoundException;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class BestManagerTest {

    @Mock
    IUserAccessor userAccessor;

    @Mock
    IBestConverter bestConverter;

    @InjectMocks
    BestManager bestManager;

    private DomainUser domainUser;
    private DomainBest domainBest;
    private ViewBest viewBest;

    @Before
    public void setUp() throws Exception {
        domainUser = new DomainUser();
        domainBest = new DomainBest();
        viewBest = new ViewBest();

        domainUser.setDomainBest(domainBest);
    }

    @Test
    public void getCurrentPersonalBest() {
        when(userAccessor.findById(anyInt()))
                .thenReturn(Optional.of(domainUser));
        when(bestConverter.domainToView(any(DomainBest.class)))
                .thenReturn(viewBest);

        ViewBest result = bestManager.getCurrentPersonalBest(0);
        assertEquals(viewBest, result);
    }

    @Test(expected = EntityNotFoundException.class)
    public void getCurrentPersonalBest_NoUserWithProvidedId() {
        when(userAccessor.findById(anyInt()))
                .thenReturn(Optional.empty());

        bestManager.getCurrentPersonalBest(0);
    }
}