package com.dph.runtracker.converters.impl;

import com.dph.runtracker.accessors.IUserAccessor;
import com.dph.runtracker.converters.ILocalDateEpochConverter;
import com.dph.runtracker.domains.DomainUser;
import com.dph.runtracker.views.ViewUser;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserConverterTest {

    @Mock
    private ILocalDateEpochConverter localDateEpochConverter;

    @Mock
    private IUserAccessor userAccessor;

    @InjectMocks
    private UserConverter userConverter;

    private DomainUser domainUser;
    private ViewUser viewUser;
    private LocalDate y2kLocalDate;
    private Long y2kLong;

    @Before
    public void setUp() throws Exception {
        y2kLocalDate = LocalDate.of(2000, 1, 1);
        y2kLong = 946706400000L; // Unix time of Local Date Y2K
        domainUser = new DomainUser();
        viewUser = new ViewUser();
    }

    @Test
    public void viewToDomain_NewUser() {
        when(localDateEpochConverter.convertMillisToLocalDate(anyLong()))
                .thenReturn(y2kLocalDate);

        // Not setting the Id... null Id means new user
        viewUser.setUsername("ATestUsername");
        viewUser.setDob(y2kLong);
        viewUser.setFirstName("John");
        viewUser.setLastName("Doe");
        viewUser.setGender("male");
        viewUser.setHeight(60f);
        viewUser.setWeight(200f);

        DomainUser result = userConverter.viewToDomain(viewUser);

        assertEquals("ATestUsername", result.getUsername());
        assertEquals(y2kLocalDate, result.getDob());
        assertEquals("John", result.getFirstName());
        assertEquals("Doe", result.getLastName());
        assertEquals("male", result.getGender());
        assertEquals(60f, result.getHeight(), 0.01f);
        assertEquals(200f, result.getWeight(), 0.01f);

        LocalDateTime createdOnLDT = result.getCreatedOn();
        LocalDateTime modifiedOnLDT = result.getModifiedOn();

        assertNotNull(createdOnLDT);
        assertNotNull(modifiedOnLDT);
        assertTrue(createdOnLDT.equals(modifiedOnLDT));
    }

    @Test
    public void viewToDomain_ExistingUser() {
        when(localDateEpochConverter.convertMillisToLocalDate(anyLong()))
                .thenReturn(y2kLocalDate);
        when(userAccessor.findById(anyInt()))
                .thenReturn(Optional.of(domainUser));

        // Giving the returned domain user an Id and created on time
        // because it already exists
        domainUser.setId(100);
        domainUser.setCreatedOn(LocalDateTime.of(2000, 1, 1, 1, 1, 1));

        viewUser.setId(100);
        viewUser.setUsername("ATestUsername");
        viewUser.setDob(y2kLong);
        viewUser.setFirstName("John");
        viewUser.setLastName("Doe");
        viewUser.setGender("male");
        viewUser.setHeight(60f);
        viewUser.setWeight(200f);

        DomainUser result = userConverter.viewToDomain(viewUser);

        assertEquals(100, (int)result.getId());
        assertEquals("ATestUsername", result.getUsername());
        assertEquals(y2kLocalDate, result.getDob());
        assertEquals("John", result.getFirstName());
        assertEquals("Doe", result.getLastName());
        assertEquals("male", result.getGender());
        assertEquals(60f, result.getHeight(), 0.01f);
        assertEquals(200f, result.getWeight(), 0.01f);

        LocalDateTime createdOnLDT = result.getCreatedOn();
        LocalDateTime modifiedOnLDT = result.getModifiedOn();

        assertNotNull(createdOnLDT);
        assertNotNull(modifiedOnLDT);
        assertFalse(createdOnLDT.equals(modifiedOnLDT));
    }

    @Test(expected = EntityNotFoundException.class)
    public void viewToDomain_NoUserWithGivenViewId() {
        when(userAccessor.findById(anyInt()))
                .thenReturn(Optional.empty());

        viewUser.setId(9999);

        userConverter.viewToDomain(viewUser);
    }

    @Test
    public void domainToView() {
        when(localDateEpochConverter.convertLocalDateToMillis(any(LocalDate.class)))
                .thenReturn(y2kLong);

        domainUser.setId(99);
        domainUser.setUsername("AnotherTestUsername");
        domainUser.setDob(y2kLocalDate);
        domainUser.setFirstName("Jane");
        domainUser.setLastName("Doll");
        domainUser.setGender("female");
        domainUser.setHeight(50f);
        domainUser.setWeight(120f);

        ViewUser result = userConverter.domainToView(domainUser);

        assertEquals(99, (int)result.getId());
        assertEquals("AnotherTestUsername", result.getUsername());
        assertEquals(y2kLong, result.getDob());
        assertEquals("Jane", result.getFirstName());
        assertEquals("Doll", result.getLastName());
        assertEquals("female", result.getGender());
        assertEquals(50f, result.getHeight(), 0.01f);
        assertEquals(120f, result.getWeight(), 0.01f);
    }
}