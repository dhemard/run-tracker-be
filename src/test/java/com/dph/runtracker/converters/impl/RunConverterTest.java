package com.dph.runtracker.converters.impl;

import com.dph.runtracker.accessors.IRunAccessor;
import com.dph.runtracker.accessors.IUserAccessor;
import com.dph.runtracker.converters.ILocalDateTimeEpochConverter;
import com.dph.runtracker.domains.DomainRun;
import com.dph.runtracker.domains.DomainUser;
import com.dph.runtracker.engines.IBestEngine;
import com.dph.runtracker.engines.IRunEngine;
import com.dph.runtracker.views.ViewRun;
import com.dph.runtracker.views.ViewUser;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDateTime;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RunConverterTest {

    @Mock
    private IUserAccessor userAccessor;

    @Mock
    private IRunAccessor runAccessor;

    @Mock
    private ILocalDateTimeEpochConverter localDateTimeEpochConverter;

    @Mock
    private IRunEngine runEngine;

    @Mock
    private IBestEngine bestEngine;

    @InjectMocks
    private RunConverter runConverter;

    private DomainUser domainUser;
    private ViewUser viewUser;
    private DomainRun domainRun;
    private ViewRun viewRun;
    private LocalDateTime elevenElevenLDT;
    private Long elevenElevenLong;

    @Before
    public void setUp() throws Exception {
        domainUser = new DomainUser();
        viewUser = new ViewUser();
        domainRun = new DomainRun();
        viewRun = new ViewRun();
        elevenElevenLDT = LocalDateTime.of(2011, 11, 11, 11, 11, 11);
        elevenElevenLong = 1321031471000L; // Unix time of Local 2011-11-11 11:11:11
    }

    @Test
    public void viewToDomain_NewRun_UserExists() {
        when(userAccessor.findById(anyInt()))
                .thenReturn(Optional.of(domainUser));
        when(localDateTimeEpochConverter.convertMillisToLocalDateTime(anyLong()))
                .thenReturn(elevenElevenLDT);
        doNothing().when(bestEngine).updatePersonalBests(any(DomainUser.class), any(ViewRun.class));

        domainUser.setId(100);
        // Not setting the run Id... null Id means new run
        viewRun.setUserId(100);
        viewRun.setStartTime(elevenElevenLong);
        viewRun.setElapsed(500f);
        viewRun.setDistance(1f);
        viewRun.setRating(3);

        DomainRun result = runConverter.viewToDomain(viewRun);

        assertEquals(domainUser, result.getDomainUser());
        assertEquals(elevenElevenLDT, result.getStartTime());
        assertEquals(500f, result.getElapsed(), 0.01f);
        assertEquals(1f, result.getDistance(), 0.01f);
        assertEquals(3, (int)result.getRating());

        LocalDateTime createdOn = result.getCreatedOn();
        LocalDateTime modifiedOn = result.getModifiedOn();

        assertNotNull(createdOn);
        assertNotNull(modifiedOn);
        assertTrue(createdOn.equals(modifiedOn));
    }

    @Test(expected = EntityNotFoundException.class)
    public void viewToDomain_NewRun_NoUserWithGivenViewUserId() {
        when(userAccessor.findById(anyInt()))
                .thenReturn(Optional.empty());

        viewRun.setUserId(9999);

        runConverter.viewToDomain(viewRun);
    }

    @Test
    public void viewToDomain_ExistingRun_UserExists() {
        when(localDateTimeEpochConverter.convertMillisToLocalDateTime(anyLong()))
                .thenReturn(elevenElevenLDT);
        when(runAccessor.findById(anyInt()))
                .thenReturn(Optional.of(domainRun));
        doNothing().when(bestEngine).updatePersonalBests(any(DomainUser.class), any(ViewRun.class));

        domainUser.setId(100);
        // Giving the returned domain run an Id and created on time
        // and a Domain User because it already exists
        domainRun.setId(10);
        domainRun.setCreatedOn(LocalDateTime.of(2000, 1, 1, 1, 1, 1));
        domainRun.setDomainUser(domainUser);

        viewRun.setId(10);
        viewRun.setUserId(100);
        viewRun.setStartTime(elevenElevenLong);
        viewRun.setElapsed(500f);
        viewRun.setDistance(1f);
        viewRun.setRating(3);

        DomainRun result = runConverter.viewToDomain(viewRun);

        assertEquals(domainUser, result.getDomainUser());
        assertEquals(elevenElevenLDT, result.getStartTime());
        assertEquals(500f, result.getElapsed(), 0.01f);
        assertEquals(1f, result.getDistance(), 0.01f);
        assertEquals(3, (int)result.getRating());

        LocalDateTime createdOn = result.getCreatedOn();
        LocalDateTime modifiedOn = result.getModifiedOn();

        assertNotNull(createdOn);
        assertNotNull(modifiedOn);
        assertFalse(createdOn.equals(modifiedOn));
    }

    @Test(expected = EntityNotFoundException.class)
    public void viewToDomain_ExistingRun_NoRunWithGivenViewId() {
        when(runAccessor.findById(anyInt()))
                .thenReturn(Optional.empty());

        viewRun.setId(8888);

        runConverter.viewToDomain(viewRun);
    }

    @Test
    public void domainToView() {
        when(localDateTimeEpochConverter.convertLocalDateTimeToMillis(elevenElevenLDT))
                .thenReturn(elevenElevenLong);
        when(runEngine.calculateAveragePace(anyFloat(), anyFloat()))
                .thenReturn(6.67f);

        domainRun.setId(50);
        // Give the user a user ID
        domainUser.setId(5);
        domainRun.setDomainUser(domainUser);
        domainRun.setStartTime(elevenElevenLDT);
        domainRun.setElapsed(600f);
        domainRun.setDistance(1.5f);
        domainRun.setRating(5);

        ViewRun result = runConverter.domainToView(domainRun);

        assertEquals(50, (int)result.getId());
        assertEquals(5, (int)result.getUserId());
        assertEquals(elevenElevenLong, result.getStartTime());
        assertEquals(600f, result.getElapsed(), 0.01f);
        assertEquals(1.5f, result.getDistance(), 0.01f);
        assertEquals(5, (int)result.getRating());
        assertEquals(6.67f, result.getAvgPace(), 0.001);
    }
}