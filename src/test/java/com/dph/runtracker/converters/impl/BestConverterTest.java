package com.dph.runtracker.converters.impl;

import com.dph.runtracker.domains.DomainBest;
import com.dph.runtracker.domains.DomainUser;
import com.dph.runtracker.views.ViewBest;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class BestConverterTest {

    private ViewBest viewBest;
    private DomainBest domainBest;
    private DomainUser domainUser;

    private BestConverter bestConverter;

    @Before
    public void setUp() throws Exception {
        domainUser = new DomainUser();
        viewBest = new ViewBest();
        domainBest = new DomainBest();

        bestConverter = new BestConverter();
    }

    @Test
    public void domainToView() {
        domainUser.setId(0);
        domainUser.setDomainBest(domainBest);

        domainBest.setId(0);
        domainBest.setDomainUser(domainUser);
        domainBest.setDistance(10f);
        domainBest.setElapsed(6000f);
        domainBest.setAvgPace(10f);

        ViewBest result = bestConverter.domainToView(domainBest);

        assertEquals(0, (int)result.getId());
        assertEquals(0, (int)result.getUserId());
        assertEquals(10f, result.getDistance(), 0.00001f);
        assertEquals(6000f, result.getElapsed(), 0.00001f);
        assertEquals(10f, result.getAvgPace(), 0.00001f);
    }
}