package com.dph.runtracker.converters.impl;

import com.dph.runtracker.converters.ILocalDateTimeEpochConverter;
import org.junit.Test;

import java.time.LocalDateTime;

import static org.junit.Assert.*;

public class LocalDateTimeEpochConverterTest {

    ILocalDateTimeEpochConverter localDateTimeEpochConverter = new LocalDateTimeEpochConverter();

    @Test
    public void convertMillisToLocalDateTime() {
        Long epochMillis = 946684800000L;   // 2000-01-01 00:00:00 in UTC
        LocalDateTime localDateTime = localDateTimeEpochConverter
                .convertMillisToLocalDateTime(epochMillis);
        assertEquals(2000, localDateTime.getYear());
        assertEquals(1, localDateTime.getMonthValue());
        assertEquals(1, localDateTime.getDayOfMonth());
        assertEquals(0, localDateTime.getHour());
        assertEquals(0, localDateTime.getMinute());
        assertEquals(0, localDateTime.getSecond());
    }

    @Test
    public void convertMillisToLocalDateTime_NullEpoch() {
        Long epochMillis = null;
        LocalDateTime localDateTime = localDateTimeEpochConverter
                .convertMillisToLocalDateTime(epochMillis);
        assertNull(localDateTime);
    }

    @Test
    public void convertLocalDateTimeToMillis() {
        LocalDateTime localDateTime = LocalDateTime.of(2000, 1, 1, 0, 0, 0);
        Long epochMillis = localDateTimeEpochConverter
                .convertLocalDateTimeToMillis(localDateTime);
        assertEquals((Long)946684800000L, epochMillis);
    }

    @Test
    public void convertLocalDateTimeToMillis_NullLocalDateTime() {
        LocalDateTime localDateTime = null;
        Long epochMillis = localDateTimeEpochConverter
                .convertLocalDateTimeToMillis(localDateTime);
        assertNull(epochMillis);    }

}