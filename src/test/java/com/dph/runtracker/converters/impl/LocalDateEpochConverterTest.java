package com.dph.runtracker.converters.impl;

import com.dph.runtracker.converters.ILocalDateEpochConverter;
import org.junit.Test;

import java.time.LocalDate;

import static org.junit.Assert.*;

public class LocalDateEpochConverterTest {

    ILocalDateEpochConverter localDateEpochConverter = new LocalDateEpochConverter();

    @Test
    public void convertMillisToLocalDate() {
        Long epochMillis = 946684800000L;   // 2000-01-01 in UTC format
        LocalDate localDate = localDateEpochConverter
                .convertMillisToLocalDate(epochMillis);
        assertEquals(2000, localDate.getYear());
        assertEquals(1, localDate.getMonthValue());
        assertEquals(1, localDate.getDayOfMonth());
    }

    @Test
    public void convertMillisToLocalDate_NullEpoch() {
        Long epochMillis = null;
        LocalDate localDate = localDateEpochConverter
                .convertMillisToLocalDate(epochMillis);
        assertNull(localDate);
    }

    @Test
    public void convertLocalDateToMillis() {
        LocalDate localDate = LocalDate.of(2000, 1, 1);
        Long epochMillis = localDateEpochConverter
                .convertLocalDateToMillis(localDate);
        assertEquals((Long)946684800000L, epochMillis);
    }

    @Test
    public void convertLocalDateToMillis_NullLocalDate() {
        LocalDate localDate = null;
        Long epochMillis = localDateEpochConverter
                .convertLocalDateToMillis(localDate);
        assertNull(epochMillis);
    }
}