package com.dph.runtracker.engines.impl;

import com.dph.runtracker.accessors.IBestAccessor;
import com.dph.runtracker.domains.DomainBest;
import com.dph.runtracker.domains.DomainUser;
import com.dph.runtracker.engines.IRunEngine;
import com.dph.runtracker.views.ViewRun;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyFloat;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class BestEngineTest {

    @Mock
    private IRunEngine runEngine;

    @Mock
    private IBestAccessor bestAccessor;

    @InjectMocks
    private BestEngine bestEngine;

    private DomainUser domainUser;
    private ViewRun viewRun;
    private DomainBest domainBest;

    @Before
    public void setUp() throws Exception {
        domainUser = new DomainUser();
        domainBest = new DomainBest();
        domainUser.setDomainBest(domainBest);

        viewRun = new ViewRun();
        viewRun.setElapsed(600f);
        viewRun.setDistance(1f);
    }

    @Test
    public void updatePersonalBests_NullBestElapsed() {
        when(bestAccessor.save(any(DomainBest.class)))
                .thenReturn(domainBest);

        domainBest.setElapsed(null);
        domainBest.setDistance(1f);
        domainBest.setAvgPace(10f);

        assertNull(domainBest.getElapsed());
        bestEngine.updatePersonalBests(domainUser, viewRun);
        assertEquals(600f, domainBest.getElapsed(), 0.0001);
    }

    @Test
    public void updatePersonalBests_NullBestDistance() {
        when(bestAccessor.save(any(DomainBest.class)))
                .thenReturn(domainBest);

        domainBest.setElapsed(600f);
        domainBest.setDistance(null);
        domainBest.setAvgPace(10f);

        assertNull(domainBest.getDistance());
        bestEngine.updatePersonalBests(domainUser, viewRun);
        assertEquals(1f, domainBest.getDistance(), 0.0001);
    }

    @Test
    public void updatePersonalBests_NullBestAvgPace() {
        when(bestAccessor.save(any(DomainBest.class)))
                .thenReturn(domainBest);
        when(runEngine.calculateAveragePace(anyFloat(), anyFloat()))
                .thenReturn(10f);

        domainBest.setElapsed(600f);
        domainBest.setDistance(1f);
        domainBest.setAvgPace(null);

        assertNull(domainBest.getAvgPace());
        bestEngine.updatePersonalBests(domainUser, viewRun);
        assertEquals(10f, domainBest.getAvgPace(), 0.0001);
    }

    @Test
    public void updatePersonalBests_BeatenBestElapsed() {
        when(bestAccessor.save(any(DomainBest.class)))
                .thenReturn(domainBest);
        when(runEngine.calculateAveragePace(anyFloat(), anyFloat()))
                .thenReturn(10f);

        domainBest.setElapsed(599f);
        domainBest.setDistance(1f);
        domainBest.setAvgPace(10f);

        assertEquals(599f, domainBest.getElapsed(), 0.0001);
        bestEngine.updatePersonalBests(domainUser, viewRun);
        assertEquals(600f, domainBest.getElapsed(), 0.0001);
    }

    @Test
    public void updatePersonalBests_BeatenBestDistance() {
        when(bestAccessor.save(any(DomainBest.class)))
                .thenReturn(domainBest);
        when(runEngine.calculateAveragePace(anyFloat(), anyFloat()))
                .thenReturn(10f);

        domainBest.setElapsed(600f);
        domainBest.setDistance(0.9f);
        domainBest.setAvgPace(10f);

        assertEquals(0.9f, domainBest.getDistance(), 0.0001);
        bestEngine.updatePersonalBests(domainUser, viewRun);
        assertEquals(1f, domainBest.getDistance(), 0.0001);
    }

    @Test
    public void updatePersonalBests_BeatenBestAvgPace() {
        when(bestAccessor.save(any(DomainBest.class)))
                .thenReturn(domainBest);
        when(runEngine.calculateAveragePace(anyFloat(), anyFloat()))
                .thenReturn(10f);

        domainBest.setElapsed(600f);
        domainBest.setDistance(1f);
        domainBest.setAvgPace(11f);

        assertEquals(11f, domainBest.getAvgPace(), 0.0001);
        bestEngine.updatePersonalBests(domainUser, viewRun);
        assertEquals(10f, domainBest.getAvgPace(), 0.0001);
    }

    @Test
    public void updatePersonalBests_UnbeatenBestElapsed() {
        when(runEngine.calculateAveragePace(anyFloat(), anyFloat()))
                .thenReturn(10f);

        domainBest.setElapsed(601f);
        domainBest.setDistance(1f);
        domainBest.setAvgPace(10f);

        assertEquals(601f, domainBest.getElapsed(), 0.0001);
        bestEngine.updatePersonalBests(domainUser, viewRun);
        assertEquals(601f, domainBest.getElapsed(), 0.0001);
    }

    @Test
    public void updatePersonalBests_UnbeatenBestDistance() {
        when(runEngine.calculateAveragePace(anyFloat(), anyFloat()))
                .thenReturn(10f);

        domainBest.setElapsed(600f);
        domainBest.setDistance(1.1f);
        domainBest.setAvgPace(10f);

        assertEquals(1.1f, domainBest.getDistance(), 0.0001);
        bestEngine.updatePersonalBests(domainUser, viewRun);
        assertEquals(1.1f, domainBest.getDistance(), 0.0001);
    }

    @Test
    public void updatePersonalBests_UnbeatenBestAvgPace() {
        when(runEngine.calculateAveragePace(anyFloat(), anyFloat()))
                .thenReturn(10f);

        domainBest.setElapsed(600f);
        domainBest.setDistance(1f);
        domainBest.setAvgPace(9.9f);

        assertEquals(9.9f, domainBest.getAvgPace(), 0.0001);
        bestEngine.updatePersonalBests(domainUser, viewRun);
        assertEquals(9.9f, domainBest.getAvgPace(), 0.0001);
    }
}