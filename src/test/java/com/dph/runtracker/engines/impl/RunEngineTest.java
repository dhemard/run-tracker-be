package com.dph.runtracker.engines.impl;

import com.dph.runtracker.domains.DomainRun;
import com.dph.runtracker.engines.IRunEngine;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class RunEngineTest {

    private IRunEngine runEngine;

    private DomainRun domainRun;

    @Before
    public void setUp() throws Exception {
        runEngine = new RunEngine();

        domainRun = new DomainRun();
        domainRun.setDistance(6.21371f);
        domainRun.setElapsed(3510f);
    }

    @Test
    public void calculateAveragePace() {
        Float result = runEngine.calculateAveragePace(
                domainRun.getElapsed(),
                domainRun.getDistance()
        );

        assertEquals(9.415, (float)result, 0.001);
    }
}