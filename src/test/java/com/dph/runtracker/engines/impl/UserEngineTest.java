package com.dph.runtracker.engines.impl;

import com.dph.runtracker.accessors.IUserAccessor;
import com.dph.runtracker.domains.DomainUser;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserEngineTest {

    @Mock
    private IUserAccessor userAccessor;

    @InjectMocks
    private UserEngine userEngine;

    private DomainUser allUpperUser;
    private DomainUser mixedCaseUser;

    @Before
    public void setUp() {
        allUpperUser = new DomainUser();
        allUpperUser.setId(9998);
        allUpperUser.setUsername("UPPERCASEUSERNAME");

        mixedCaseUser = new DomainUser();
        mixedCaseUser.setId(9999);
        mixedCaseUser.setUsername("MiXeDcAsEuSeRnAmE");
    }

    @Test
    public void usernameIsUnique_HappyPath() {
        when(userAccessor.findDistinctByUsernameIgnoreCase(anyString()))
                .thenReturn(null);

        assertTrue(userEngine.usernameIsUnique("MyUsername"));
    }

    @Test
    public void usernameIsUnique_ExactMatchToExistingUsername() {
        when(userAccessor.findDistinctByUsernameIgnoreCase(anyString()))
                .thenReturn(mixedCaseUser);

        assertFalse(userEngine.usernameIsUnique("MiXeDcAsEuSeRnAmE"));
    }

    @Test
    public void usernameIsUnique_DifferentCaseAsExistingUsername() {
        when(userAccessor.findDistinctByUsernameIgnoreCase(anyString()))
                .thenReturn(allUpperUser);

        assertFalse(userEngine.usernameIsUnique("uppercaseusername"));
    }
}