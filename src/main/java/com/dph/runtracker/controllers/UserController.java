package com.dph.runtracker.controllers;

import com.dph.runtracker.managers.IBestManager;
import com.dph.runtracker.managers.IUserManager;
import com.dph.runtracker.views.ViewBest;
import com.dph.runtracker.views.ViewUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/users")
public class UserController {

    @Autowired
    private IUserManager userManager;

    @Autowired
    private IBestManager bestManager;

    @RequestMapping(method = RequestMethod.GET)
    ResponseEntity<List<ViewUser>> getAllUsers() {
        return new ResponseEntity<>(userManager.getAllUsers(), HttpStatus.OK);
    }

    @RequestMapping(value = "/{userId}", method = RequestMethod.GET)
    ResponseEntity<ViewUser> getUserById(@PathVariable Integer userId){
        return new ResponseEntity<>(userManager.getUserById(userId), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    ResponseEntity<ViewUser> createUser(@RequestBody ViewUser viewUser){
        return new ResponseEntity<>(userManager.createUser(viewUser), HttpStatus.OK);
    }

    @RequestMapping(value = "/{userId}", method = RequestMethod.PUT)
    ResponseEntity<ViewUser> updateUser(
            @PathVariable Integer userId, @RequestBody ViewUser viewUser){
        return new ResponseEntity<>(userManager.updateUser(userId, viewUser), HttpStatus.OK);
    }

    @RequestMapping(value = "/{userId}", method = RequestMethod.DELETE)
    ResponseEntity<ViewUser> deleteUser(@PathVariable Integer userId){
        return new ResponseEntity<>(userManager.deleteUser(userId), HttpStatus.OK);
    }

    @RequestMapping(value = "/{userId}/bests", method = RequestMethod.GET)
    ResponseEntity<ViewBest> getPersonalBest(@PathVariable Integer userId){
        return new ResponseEntity<>(bestManager.getCurrentPersonalBest(userId), HttpStatus.OK);
    }
}
