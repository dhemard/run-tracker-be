package com.dph.runtracker.controllers;

import com.dph.runtracker.managers.IRunManager;
import com.dph.runtracker.views.ViewRun;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/api/v1/users/{userId}/runs")
public class RunController {

    @Autowired
    private IRunManager runManager;

    @RequestMapping(method = RequestMethod.GET)
    ResponseEntity<List<ViewRun>> getAllRuns(@PathVariable Integer userId) {
        return new ResponseEntity<>(runManager.getAllRuns(userId), HttpStatus.OK);
    }

    @RequestMapping(value = "/{runId}", method = RequestMethod.GET)
    ResponseEntity<ViewRun> getRunById(@PathVariable Integer userId,
                                       @PathVariable Integer runId) {
        return new ResponseEntity<>(runManager.getRunById(userId, runId), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    ResponseEntity<ViewRun> createRun(@PathVariable Integer userId,
                                      @RequestBody ViewRun viewRun) {
        return new ResponseEntity<>(runManager.createRun(userId, viewRun), HttpStatus.OK);
    }

    @RequestMapping(value = "/{runId}", method = RequestMethod.PUT)
    ResponseEntity<ViewRun> updateRun(@PathVariable Integer userId,
                                      @PathVariable Integer runId,
                                      @RequestBody ViewRun viewRun) {
        return new ResponseEntity<>(runManager.updateRun(userId,runId, viewRun), HttpStatus.OK);
    }

    @RequestMapping(value = "/{runId}", method = RequestMethod.DELETE)
    ResponseEntity<ViewRun> deleteRun(@PathVariable Integer userId,
                                      @PathVariable Integer runId) {
        return new ResponseEntity<>(runManager.deleteRun(userId, runId), HttpStatus.OK);
    }
}
