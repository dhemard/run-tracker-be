package com.dph.runtracker.domains;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Objects;

/* Using Soft Deletion implemented with Hibernate
 * See: https://vladmihalcea.com/the-best-way-to-soft-delete-with-hibernate/
 * and  https://www.thoughts-on-java.org/implement-soft-delete-hibernate/ */

/* Using One to Many relationship
 * See: https://www.callicoder.com/hibernate-spring-boot-jpa-one-to-many-mapping-example/
 */

@Entity(name = "DomainUser")
@Table(name = "user")
@SQLDelete(sql =
        "UPDATE user " +
        "SET deleted = TRUE " +
        "WHERE id = ?")
@Where(clause = "deleted = false")
public class DomainUser {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column
    private String username;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column
    private String gender;

    @Column
    private Float height;

    @Column
    private Float weight;

    @Column
    private LocalDate dob;

    @Column(name = "created_on")
    private LocalDateTime createdOn;

    @Column(name = "modified_on")
    private LocalDateTime modifiedOn;

    @OneToOne(fetch = FetchType.LAZY,
            cascade = CascadeType.ALL,
            mappedBy = "domainUser")
    private DomainBest domainBest;

    @Column
    private boolean deleted;

    //TODO: debug cascade soft delete

    @PreRemove
    public void deleteUser() {
        this.deleted = true;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Float getHeight() {
        return height;
    }

    public void setHeight(Float height) {
        this.height = height;
    }

    public Float getWeight() {
        return weight;
    }

    public void setWeight(Float weight) {
        this.weight = weight;
    }

    public LocalDate getDob() {
        return dob;
    }

    public void setDob(LocalDate dob) {
        this.dob = dob;
    }

    public LocalDateTime getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(LocalDateTime createdOn) {
        this.createdOn = createdOn;
    }

    public LocalDateTime getModifiedOn() {
        return modifiedOn;
    }

    public void setModifiedOn(LocalDateTime modifiedOn) {
        this.modifiedOn = modifiedOn;
    }

    public DomainBest getDomainBest() {
        return domainBest;
    }

    public void setDomainBest(DomainBest domainBest) {
        this.domainBest = domainBest;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DomainUser that = (DomainUser) o;
        return deleted == that.deleted &&
                Objects.equals(id, that.id) &&
                Objects.equals(username, that.username) &&
                Objects.equals(firstName, that.firstName) &&
                Objects.equals(lastName, that.lastName) &&
                Objects.equals(gender, that.gender) &&
                Objects.equals(height, that.height) &&
                Objects.equals(weight, that.weight) &&
                Objects.equals(dob, that.dob) &&
                Objects.equals(createdOn, that.createdOn) &&
                Objects.equals(modifiedOn, that.modifiedOn) &&
                Objects.equals(domainBest, that.domainBest);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, username, firstName, lastName, gender, height, weight, dob, createdOn, modifiedOn, domainBest, deleted);
    }

    @Override
    public String toString() {
        return "DomainUser{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", gender='" + gender + '\'' +
                ", height=" + height +
                ", weight=" + weight +
                ", dob=" + dob +
                ", createdOn=" + createdOn +
                ", modifiedOn=" + modifiedOn +
                ", domainBest=" + domainBest +
                ", deleted=" + deleted +
                '}';
    }
}
