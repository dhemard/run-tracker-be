package com.dph.runtracker.domains;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "best")
public class DomainBest {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    private DomainUser domainUser;

    @Column
    private Float distance;

    @Column
    private Float elapsed;

    @Column(name = "avg_pace")
    private Float avgPace;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public DomainUser getDomainUser() {
        return domainUser;
    }

    public void setDomainUser(DomainUser domainUser) {
        this.domainUser = domainUser;
    }

    public Float getDistance() {
        return distance;
    }

    public void setDistance(Float distance) {
        this.distance = distance;
    }

    public Float getElapsed() {
        return elapsed;
    }

    public void setElapsed(Float elapsed) {
        this.elapsed = elapsed;
    }

    public Float getAvgPace() {
        return avgPace;
    }

    public void setAvgPace(Float avgPace) {
        this.avgPace = avgPace;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DomainBest that = (DomainBest) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(domainUser, that.domainUser) &&
                Objects.equals(distance, that.distance) &&
                Objects.equals(elapsed, that.elapsed) &&
                Objects.equals(avgPace, that.avgPace);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, domainUser, distance, elapsed, avgPace);
    }

    @Override
    public String toString() {
        return "DomainBest{" +
                "id=" + id +
                ", domainUser=" + domainUser +
                ", distance=" + distance +
                ", elapsed=" + elapsed +
                ", avgPace=" + avgPace +
                '}';
    }
}
