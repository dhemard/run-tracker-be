package com.dph.runtracker.domains;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;

/* Using Soft Deletion implemented with Hibernate
 * See: https://vladmihalcea.com/the-best-way-to-soft-delete-with-hibernate/
 * and  https://www.thoughts-on-java.org/implement-soft-delete-hibernate/
 */

/* Using Many to One relationship
 * See: https://www.callicoder.com/hibernate-spring-boot-jpa-one-to-many-mapping-example/
 */

@Entity
@Table(name = "run")
@SQLDelete(sql =
        "UPDATE run " +
        "SET deleted = TRUE " +
        "WHERE id = ?")
@Where(clause = "deleted = false")
public class DomainRun {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", referencedColumnName = "id", nullable = false)
    private DomainUser domainUser;

    @Column(name = "start_time")
    private LocalDateTime startTime;

    @Column
    private Float elapsed;

    @Column
    private Float distance;

    @Column
    private Integer rating;

    @Column(name = "created_on")
    private LocalDateTime createdOn;

    @Column(name = "modified_on")
    private LocalDateTime modifiedOn;

    @Column
    private boolean deleted;

    @PreRemove
    public void deleteRun() {
        this.deleted = true;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public DomainUser getDomainUser() {
        return domainUser;
    }

    public void setDomainUser(DomainUser domainUser) {
        this.domainUser = domainUser;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public Float getElapsed() {
        return elapsed;
    }

    public void setElapsed(Float elapsed) {
        this.elapsed = elapsed;
    }

    public Float getDistance() {
        return distance;
    }

    public void setDistance(Float distance) {
        this.distance = distance;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public LocalDateTime getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(LocalDateTime createdOn) {
        this.createdOn = createdOn;
    }

    public LocalDateTime getModifiedOn() {
        return modifiedOn;
    }

    public void setModifiedOn(LocalDateTime modifiedOn) {
        this.modifiedOn = modifiedOn;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DomainRun domainRun = (DomainRun) o;
        return deleted == domainRun.deleted &&
                Objects.equals(id, domainRun.id) &&
                Objects.equals(domainUser, domainRun.domainUser) &&
                Objects.equals(startTime, domainRun.startTime) &&
                Objects.equals(elapsed, domainRun.elapsed) &&
                Objects.equals(distance, domainRun.distance) &&
                Objects.equals(rating, domainRun.rating) &&
                Objects.equals(createdOn, domainRun.createdOn) &&
                Objects.equals(modifiedOn, domainRun.modifiedOn);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, domainUser, startTime, elapsed, distance, rating, createdOn, modifiedOn, deleted);
    }

    @Override
    public String toString() {
        return "DomainRun{" +
                "id=" + id +
                ", domainUser=" + domainUser +
                ", startTime=" + startTime +
                ", elapsed=" + elapsed +
                ", distance=" + distance +
                ", rating=" + rating +
                ", createdOn=" + createdOn +
                ", modifiedOn=" + modifiedOn +
                ", deleted=" + deleted +
                '}';
    }
}
