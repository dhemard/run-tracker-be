package com.dph.runtracker.engines;

import com.dph.runtracker.domains.DomainUser;
import com.dph.runtracker.views.ViewRun;

public interface IBestEngine {
    void updatePersonalBests(DomainUser domainUser, ViewRun viewRun);
}
