package com.dph.runtracker.engines;

public interface IUserEngine {
    boolean usernameIsUnique(String desiredUsername);
}
