package com.dph.runtracker.engines;

public interface IRunEngine {
    float calculateAveragePace(float elapsedSeconds, float distanceInMiles);
}
