package com.dph.runtracker.engines.impl;

import com.dph.runtracker.accessors.IBestAccessor;
import com.dph.runtracker.domains.DomainBest;
import com.dph.runtracker.domains.DomainUser;
import com.dph.runtracker.engines.IBestEngine;
import com.dph.runtracker.engines.IRunEngine;
import com.dph.runtracker.views.ViewRun;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BestEngine implements IBestEngine {

    @Autowired
    private IRunEngine runEngine;

    @Autowired
    private IBestAccessor bestAccessor;

    @Override
    public void updatePersonalBests(DomainUser domainUser, ViewRun viewRun) {
        // Calculate this run's Avg Pace
        Float avgPace = runEngine.calculateAveragePace(viewRun.getElapsed(), viewRun.getDistance());

        // If this new run set a personal best, Update the user's Best
        DomainBest domainBest = domainUser.getDomainBest();
        boolean newBest = false;
        if (domainBest.getElapsed() == null ||
                domainBest.getElapsed().compareTo(viewRun.getElapsed()) < 0) {
            // This run was longer (time) than the user's longest run
            domainBest.setElapsed(viewRun.getElapsed());
            newBest = true;
        }
        if (domainBest.getDistance() == null ||
                domainBest.getDistance().compareTo(viewRun.getDistance()) < 0) {
            // This run was farther than the user's farthest run
            domainBest.setDistance(viewRun.getDistance());
            newBest = true;
        }
        if (domainBest.getAvgPace() == null || domainBest.getAvgPace().compareTo(avgPace) > 0) {
            // This run's average pace was faster than the user's fastest average pace
            domainBest.setAvgPace(avgPace);
            newBest = true;
        }
        // If a new best was set, save it in the Database
        if (newBest)
            bestAccessor.save(domainBest);
    }
}
