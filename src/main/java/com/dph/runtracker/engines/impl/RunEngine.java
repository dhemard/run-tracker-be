package com.dph.runtracker.engines.impl;

import com.dph.runtracker.engines.IRunEngine;
import org.springframework.stereotype.Service;

@Service
public class RunEngine implements IRunEngine {

    @Override
    public float calculateAveragePace(float elapsedSeconds, float distanceInMiles) {
        float elapsedMinutes = elapsedSeconds / 60f;
        return elapsedMinutes / distanceInMiles;
    }
}
