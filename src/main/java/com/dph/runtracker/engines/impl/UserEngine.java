package com.dph.runtracker.engines.impl;

import com.dph.runtracker.accessors.IUserAccessor;
import com.dph.runtracker.domains.DomainUser;
import com.dph.runtracker.engines.IUserEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserEngine implements IUserEngine {
    @Autowired
    private IUserAccessor userAccessor;

    @Override
    public boolean usernameIsUnique(String desiredUsername) {
        DomainUser domainUser = userAccessor
                .findDistinctByUsernameIgnoreCase(desiredUsername);
        return (domainUser == null);
    }
}
