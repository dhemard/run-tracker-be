package com.dph.runtracker.accessors;

import com.dph.runtracker.domains.DomainBest;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IBestAccessor extends JpaRepository<DomainBest, Integer> {
}
