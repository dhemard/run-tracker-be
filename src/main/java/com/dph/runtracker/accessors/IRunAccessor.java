package com.dph.runtracker.accessors;

import com.dph.runtracker.domains.DomainRun;
import com.dph.runtracker.domains.DomainUser;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IRunAccessor extends JpaRepository<DomainRun, Integer> {
    List<DomainRun> findAllByDomainUser(DomainUser domainUser);
}
