package com.dph.runtracker.accessors;

import com.dph.runtracker.domains.DomainUser;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IUserAccessor extends JpaRepository<DomainUser, Integer> {
    DomainUser findDistinctByUsernameIgnoreCase(String username);
}
