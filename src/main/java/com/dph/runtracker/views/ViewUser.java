package com.dph.runtracker.views;

import java.util.Objects;

public class ViewUser {

    private Integer id;
    private String username;
    private String firstName;
    private String lastName;
    private String gender;
    private Float height;
    private Float weight;
    private Long dob;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Float getHeight() {
        return height;
    }

    public void setHeight(Float height) {
        this.height = height;
    }

    public Float getWeight() {
        return weight;
    }

    public void setWeight(Float weight) {
        this.weight = weight;
    }

    public Long getDob() {
        return dob;
    }

    public void setDob(Long dob) {
        this.dob = dob;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ViewUser viewUser = (ViewUser) o;
        return Objects.equals(id, viewUser.id) &&
                Objects.equals(username, viewUser.username) &&
                Objects.equals(firstName, viewUser.firstName) &&
                Objects.equals(lastName, viewUser.lastName) &&
                Objects.equals(gender, viewUser.gender) &&
                Objects.equals(height, viewUser.height) &&
                Objects.equals(weight, viewUser.weight) &&
                Objects.equals(dob, viewUser.dob);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, username, firstName, lastName, gender, height, weight, dob);
    }

    @Override
    public String toString() {
        return "ViewUser{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", gender='" + gender + '\'' +
                ", height=" + height +
                ", weight=" + weight +
                ", dob=" + dob +
                '}';
    }
}
