package com.dph.runtracker.views;

import java.util.Objects;

public class ViewBest {
    private Integer id;
    private Integer userId;
    private Float distance;
    private Float elapsed;
    private Float avgPace;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Float getDistance() {
        return distance;
    }

    public void setDistance(Float distance) {
        this.distance = distance;
    }

    public Float getElapsed() {
        return elapsed;
    }

    public void setElapsed(Float elapsed) {
        this.elapsed = elapsed;
    }

    public Float getAvgPace() {
        return avgPace;
    }

    public void setAvgPace(Float avgPace) {
        this.avgPace = avgPace;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ViewBest viewBest = (ViewBest) o;
        return Objects.equals(id, viewBest.id) &&
                Objects.equals(userId, viewBest.userId) &&
                Objects.equals(distance, viewBest.distance) &&
                Objects.equals(elapsed, viewBest.elapsed) &&
                Objects.equals(avgPace, viewBest.avgPace);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, userId, distance, elapsed, avgPace);
    }

    @Override
    public String toString() {
        return "ViewBest{" +
                "id=" + id +
                ", userId=" + userId +
                ", distance=" + distance +
                ", elapsed=" + elapsed +
                ", avgPace=" + avgPace +
                '}';
    }
}
