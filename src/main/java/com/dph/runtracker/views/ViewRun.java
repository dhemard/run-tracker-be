package com.dph.runtracker.views;

import java.util.Objects;

public class ViewRun {

    private Integer id;
    private Integer userId;
    private Long startTime;
    private Float elapsed;
    private Float distance;
    private Integer rating;
    private Float avgPace;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Float getElapsed() {
        return elapsed;
    }

    public void setElapsed(Float elapsed) {
        this.elapsed = elapsed;
    }

    public Float getDistance() {
        return distance;
    }

    public void setDistance(Float distance) {
        this.distance = distance;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public Float getAvgPace() {
        return avgPace;
    }

    public void setAvgPace(Float avgPace) {
        this.avgPace = avgPace;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ViewRun viewRun = (ViewRun) o;
        return Objects.equals(id, viewRun.id) &&
                Objects.equals(userId, viewRun.userId) &&
                Objects.equals(startTime, viewRun.startTime) &&
                Objects.equals(elapsed, viewRun.elapsed) &&
                Objects.equals(distance, viewRun.distance) &&
                Objects.equals(rating, viewRun.rating) &&
                Objects.equals(avgPace, viewRun.avgPace);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, userId, startTime, elapsed, distance, rating, avgPace);
    }

    @Override
    public String toString() {
        return "ViewRun{" +
                "id=" + id +
                ", userId=" + userId +
                ", startTime=" + startTime +
                ", elapsed=" + elapsed +
                ", distance=" + distance +
                ", rating=" + rating +
                ", avgPace=" + avgPace +
                '}';
    }
}
