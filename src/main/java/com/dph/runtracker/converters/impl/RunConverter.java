package com.dph.runtracker.converters.impl;

import com.dph.runtracker.accessors.IRunAccessor;
import com.dph.runtracker.accessors.IUserAccessor;
import com.dph.runtracker.converters.ILocalDateTimeEpochConverter;
import com.dph.runtracker.converters.IRunConverter;
import com.dph.runtracker.domains.DomainRun;
import com.dph.runtracker.domains.DomainUser;
import com.dph.runtracker.engines.IBestEngine;
import com.dph.runtracker.engines.IRunEngine;
import com.dph.runtracker.views.ViewRun;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

@Service
public class RunConverter implements IRunConverter {

    @Autowired
    private IUserAccessor userAccessor;

    @Autowired
    private IRunAccessor runAccessor;

    @Autowired
    private ILocalDateTimeEpochConverter localDateTimeEpochConverter;

    @Autowired
    private IRunEngine runEngine;

    @Autowired
    private IBestEngine bestEngine;

    @Override
    public DomainRun viewToDomain(ViewRun viewRun) {
        LocalDateTime now = LocalDateTime.now(ZoneOffset.UTC);
        DomainRun domainRun;
        if (viewRun.getId() == null) {
            // A creation
            domainRun = new DomainRun();
            domainRun.setId(viewRun.getId());
            DomainUser domainUser = userAccessor.findById(viewRun.getUserId())
                    .orElseThrow(EntityNotFoundException::new);
            domainRun.setDomainUser(domainUser);
            domainRun.setStartTime(
                    localDateTimeEpochConverter
                        .convertMillisToLocalDateTime(viewRun.getStartTime())
            );
            domainRun.setElapsed(viewRun.getElapsed());
            domainRun.setDistance(viewRun.getDistance());
            domainRun.setRating(viewRun.getRating());
            domainRun.setCreatedOn(now);
            domainRun.setModifiedOn(now);

            // Update the User's personal bests with the newly created run data
            bestEngine.updatePersonalBests(domainUser, viewRun);
        } else {
            // An update
            //TODO: look into only updating changed fields
            domainRun = runAccessor.findById(viewRun.getId())
                    .orElseThrow(EntityNotFoundException::new);
            domainRun.setStartTime(
                    localDateTimeEpochConverter
                        .convertMillisToLocalDateTime(viewRun.getStartTime())
            );
            domainRun.setElapsed(viewRun.getElapsed());
            domainRun.setDistance(viewRun.getDistance());
            domainRun.setRating(viewRun.getRating());
            domainRun.setModifiedOn(now);

            // Update the User's personal bests with the newly updated run data
            bestEngine.updatePersonalBests(domainRun.getDomainUser(), viewRun);
        }
        return domainRun;
    }

    @Override
    public ViewRun domainToView(DomainRun domainRun) {
        ViewRun viewRun = new ViewRun();
        viewRun.setId(domainRun.getId());
        viewRun.setUserId(domainRun.getDomainUser().getId());
        viewRun.setStartTime(
                localDateTimeEpochConverter
                    .convertLocalDateTimeToMillis(domainRun.getStartTime())
        );
        viewRun.setElapsed(domainRun.getElapsed());
        viewRun.setDistance(domainRun.getDistance());
        viewRun.setRating(domainRun.getRating());
        viewRun.setAvgPace(
                runEngine.calculateAveragePace(
                        domainRun.getElapsed(),
                        domainRun.getDistance()
                )
        );
        return viewRun;
    }
}
