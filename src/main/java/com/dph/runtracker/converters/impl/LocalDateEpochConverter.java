package com.dph.runtracker.converters.impl;

import com.dph.runtracker.converters.ILocalDateEpochConverter;
import org.springframework.stereotype.Service;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneOffset;

//TODO: Investigate Conversion troubles
@Service
public class LocalDateEpochConverter implements ILocalDateEpochConverter {

    @Override
    public LocalDate convertMillisToLocalDate(Long millis) {
        if(millis != null){
            return Instant
                    .ofEpochMilli(millis)
                    .atZone(ZoneOffset.UTC)
                    .toLocalDate();
        }
        return null;
    }

    @Override
    public Long convertLocalDateToMillis(LocalDate localDate) {
        if(localDate != null){
            return localDate
                    .atStartOfDay(ZoneOffset.UTC)
                    .toInstant()
                    .toEpochMilli();
        }
        return null;
    }
}
