package com.dph.runtracker.converters.impl;

import com.dph.runtracker.converters.ILocalDateTimeEpochConverter;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

//TODO: Investigate Conversion troubles
@Service
public class LocalDateTimeEpochConverter implements ILocalDateTimeEpochConverter {

    @Override
    public LocalDateTime convertMillisToLocalDateTime(Long millis) {
        if (millis != null) {
            return Instant.ofEpochMilli(millis)
                    .atZone(ZoneOffset.UTC)
                    .toLocalDateTime();
        }
        return null;

    }

    @Override
    public Long convertLocalDateTimeToMillis(LocalDateTime localDateTime) {
        if (localDateTime != null) {
            return localDateTime.atZone(ZoneOffset.UTC)
                    .toInstant()
                    .toEpochMilli();
        }
        return null;
    }
}
