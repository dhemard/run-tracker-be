package com.dph.runtracker.converters.impl;

import com.dph.runtracker.accessors.IUserAccessor;
import com.dph.runtracker.converters.ILocalDateEpochConverter;
import com.dph.runtracker.converters.IUserConverter;
import com.dph.runtracker.domains.DomainBest;
import com.dph.runtracker.domains.DomainUser;
import com.dph.runtracker.views.ViewUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

@Service
public class UserConverter implements IUserConverter {

    @Autowired
    private ILocalDateEpochConverter localDateEpochConverter;

    @Autowired
    private IUserAccessor userAccessor;

    @Override
    public DomainUser viewToDomain(ViewUser viewUser) {
        LocalDateTime now = LocalDateTime.now(ZoneOffset.UTC);
        DomainUser domainUser;
        if(viewUser.getId() == null){
            // will be a creation
            domainUser = new DomainUser();
            domainUser.setId(viewUser.getId());
            domainUser.setUsername(viewUser.getUsername());
            domainUser.setFirstName(viewUser.getFirstName());
            domainUser.setLastName(viewUser.getLastName());
            domainUser.setGender(viewUser.getGender());
            domainUser.setHeight(viewUser.getHeight());
            domainUser.setWeight(viewUser.getWeight());
            domainUser.setDob(
                    localDateEpochConverter
                            .convertMillisToLocalDate(viewUser.getDob()));
            domainUser.setCreatedOn(now);
            domainUser.setModifiedOn(now);

            DomainBest domainBest = new DomainBest();
            domainBest.setDomainUser(domainUser);
            domainUser.setDomainBest(domainBest);
        } else {
            // will be an update
            //TODO: look into only updating changed fields
            domainUser = userAccessor.findById(viewUser.getId())
                    .orElseThrow(EntityNotFoundException::new);
            domainUser.setUsername(viewUser.getUsername());
            domainUser.setFirstName(viewUser.getFirstName());
            domainUser.setLastName(viewUser.getLastName());
            domainUser.setGender(viewUser.getGender());
            domainUser.setHeight(viewUser.getHeight());
            domainUser.setWeight(viewUser.getWeight());
            domainUser.setDob(
                    localDateEpochConverter
                            .convertMillisToLocalDate(viewUser.getDob()));
            domainUser.setModifiedOn(now);
        }
        return domainUser;
    }

    @Override
    public ViewUser domainToView(DomainUser domainUser) {
        ViewUser viewUser = new ViewUser();
        viewUser.setId(domainUser.getId());
        viewUser.setUsername(domainUser.getUsername());
        viewUser.setFirstName(domainUser.getFirstName());
        viewUser.setLastName(domainUser.getLastName());
        viewUser.setGender(domainUser.getGender());
        viewUser.setHeight(domainUser.getHeight());
        viewUser.setWeight(domainUser.getWeight());
        viewUser.setDob(
                localDateEpochConverter
                        .convertLocalDateToMillis(domainUser.getDob()));
        return viewUser;
    }
}
