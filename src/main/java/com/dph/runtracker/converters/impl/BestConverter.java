package com.dph.runtracker.converters.impl;

import com.dph.runtracker.converters.IBestConverter;
import com.dph.runtracker.domains.DomainBest;
import com.dph.runtracker.views.ViewBest;
import org.springframework.stereotype.Service;

@Service
public class BestConverter implements IBestConverter {

    @Override
    public ViewBest domainToView(DomainBest domainBest) {
        ViewBest viewBest = new ViewBest();
        viewBest.setId(domainBest.getId());
        viewBest.setUserId(domainBest.getDomainUser().getId());
        viewBest.setDistance(domainBest.getDistance());
        viewBest.setElapsed(domainBest.getElapsed());
        viewBest.setAvgPace(domainBest.getAvgPace());
        return viewBest;
    }
}
