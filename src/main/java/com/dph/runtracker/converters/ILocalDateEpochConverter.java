package com.dph.runtracker.converters;


import java.time.LocalDate;

public interface ILocalDateEpochConverter {
    LocalDate convertMillisToLocalDate(Long millis);
    Long convertLocalDateToMillis(LocalDate localDate);
}
