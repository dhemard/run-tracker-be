package com.dph.runtracker.converters;

import com.dph.runtracker.domains.DomainBest;
import com.dph.runtracker.views.ViewBest;

public interface IBestConverter {
    ViewBest domainToView(DomainBest domainBest);
}
