package com.dph.runtracker.converters;

import com.dph.runtracker.domains.DomainRun;
import com.dph.runtracker.views.ViewRun;

public interface IRunConverter {
    DomainRun viewToDomain(ViewRun viewRun);
    ViewRun domainToView(DomainRun domainRun);
}
