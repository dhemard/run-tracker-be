package com.dph.runtracker.converters;

import com.dph.runtracker.domains.DomainUser;
import com.dph.runtracker.views.ViewUser;

public interface IUserConverter {
    DomainUser viewToDomain(ViewUser viewUser);
    ViewUser domainToView(DomainUser domainUser);
}
