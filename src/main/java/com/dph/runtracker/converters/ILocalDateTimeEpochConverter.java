package com.dph.runtracker.converters;

import java.time.LocalDateTime;

public interface ILocalDateTimeEpochConverter {

    LocalDateTime convertMillisToLocalDateTime(Long millis);
    Long convertLocalDateTimeToMillis(LocalDateTime localDateTime);
}
