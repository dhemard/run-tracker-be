package com.dph.runtracker.managers;

import com.dph.runtracker.views.ViewUser;

import java.util.List;

public interface IUserManager {

    List<ViewUser> getAllUsers();
    ViewUser getUserById(Integer userId);
    ViewUser createUser(ViewUser viewUser);
    ViewUser updateUser(Integer userId, ViewUser viewUser);
    ViewUser deleteUser(Integer userId);
}
