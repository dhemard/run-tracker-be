package com.dph.runtracker.managers.impl;

import com.dph.runtracker.accessors.IRunAccessor;
import com.dph.runtracker.accessors.IUserAccessor;
import com.dph.runtracker.converters.IRunConverter;
import com.dph.runtracker.domains.DomainRun;
import com.dph.runtracker.managers.IRunManager;
import com.dph.runtracker.views.ViewRun;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;
import java.security.InvalidParameterException;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class RunManager implements IRunManager {

    @Autowired
    private IRunAccessor runAccessor;

    @Autowired
    private IUserAccessor userAccessor;

    @Autowired
    private IRunConverter runConverter;

    @Override
    public List<ViewRun> getAllRuns(Integer userId) {
        return runAccessor.findAllByDomainUser(
                userAccessor.findById(userId)
                .orElseThrow(() -> new EntityNotFoundException(
                        "User: " + userId.toString() + " not found."
                ))
        )
                .stream()
                .map(runConverter::domainToView)
                .collect(Collectors.toList());
    }

    @Override
    public ViewRun getRunById(Integer userId, Integer runId) {
        //TODO: Some validation so that a run from a different user can't be accessed
        return runConverter.domainToView(
                runAccessor.findById(runId)
                .orElseThrow(() -> new EntityNotFoundException(
                        "Run: " + runId.toString() + " not found."
                ))
        );
    }

    @Override
    public ViewRun createRun(Integer userId, ViewRun viewRun) {
        if (viewRun.getUserId() == null) {
            throw new InvalidParameterException("Unable to create run: " +
                viewRun + " \nUserId must be provided.");
        } else if (!viewRun.getUserId().equals(userId)) {
            throw new InvalidParameterException("Provided userId: " +
                userId.toString() + " does not match userId of run: " +
                viewRun);
        }
        return runConverter.domainToView(
                runAccessor.save(
                        runConverter.viewToDomain(viewRun)));
    }

    @Override
    public ViewRun updateRun(Integer userId, Integer runId, ViewRun viewRun) {
        if (viewRun.getUserId() == null) {
            throw new InvalidParameterException("Unable to update run: " +
                    viewRun + " \nUserId must be provided.");
        } else if (!viewRun.getUserId().equals(userId)) {
            throw new InvalidParameterException("Provided userId: " +
                userId.toString() + " does not match userId of run: " +
                viewRun);
        } else if (!viewRun.getId().equals(runId)) {
            throw new InvalidParameterException("Provided run id: " +
                runId.toString() + " does not match run: " +
                viewRun);
        }
        DomainRun domainRun = runAccessor.findById(runId)
                .orElseThrow(() -> new EntityNotFoundException(
                        "Unable to retrieve run: " + runId.toString()
                ));
        // TODO: Need more validation for runs
        return runConverter.domainToView(
                runAccessor.save(
                        runConverter.viewToDomain(viewRun)));
    }

    @Override
    public ViewRun deleteRun(Integer userId, Integer runId) {
        DomainRun domainRun = runAccessor.findById(runId)
                .orElseThrow(() -> new EntityNotFoundException(
                        "Unable to retrieve run: " + runId.toString()
                ));
        if (!domainRun.getDomainUser().getId().equals(userId)) {
            throw new InvalidParameterException("Run: " + runId.toString() +
                " does not belong to User: " + userId.toString());
        }
        runAccessor.deleteById(runId);
        return runConverter.domainToView(domainRun);
    }

}