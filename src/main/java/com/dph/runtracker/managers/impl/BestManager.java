package com.dph.runtracker.managers.impl;

import com.dph.runtracker.accessors.IUserAccessor;
import com.dph.runtracker.converters.IBestConverter;
import com.dph.runtracker.domains.DomainUser;
import com.dph.runtracker.managers.IBestManager;
import com.dph.runtracker.views.ViewBest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;

@Component
public class BestManager implements IBestManager {

    @Autowired
    IUserAccessor userAccessor;

    @Autowired
    IBestConverter bestConverter;

    @Override
    public ViewBest getCurrentPersonalBest(Integer userId) {
        DomainUser domainUser = userAccessor.findById(userId)
                .orElseThrow(() -> new EntityNotFoundException(
                        "Unable to retrieve user: " + userId.toString()
                ));
        return bestConverter.domainToView(domainUser.getDomainBest());
    }
}
