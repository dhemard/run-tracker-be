package com.dph.runtracker.managers.impl;

import com.dph.runtracker.accessors.IUserAccessor;
import com.dph.runtracker.converters.IUserConverter;
import com.dph.runtracker.domains.DomainUser;
import com.dph.runtracker.engines.IUserEngine;
import com.dph.runtracker.managers.IUserManager;
import com.dph.runtracker.views.ViewUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;
import java.security.InvalidParameterException;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class UserManager implements IUserManager {

    @Autowired
    private IUserAccessor userAccessor;

    @Autowired
    private IUserConverter userConverter;

    @Autowired
    private IUserEngine userEngine;

    @Override
    public List<ViewUser> getAllUsers() {
        return userAccessor.findAll().stream()
                .map(userConverter::domainToView)
                .collect(Collectors.toList());
    }

    @Override
    public ViewUser getUserById(Integer userId) {
        return userConverter.domainToView(
                userAccessor.findById(userId)
                .orElseThrow(
                        () -> new EntityNotFoundException(
                                "Unable to retrieve user: " + userId.toString()
                        )
                )
        );
    }

    @Override
    public ViewUser createUser(ViewUser viewUser) {
        if(!userEngine.usernameIsUnique(viewUser.getUsername())) {
            throw new InvalidParameterException("Username '" +
                    viewUser.getUsername() + "' already exists.");
        }
        return userConverter.domainToView(
                userAccessor.save(
                        userConverter.viewToDomain(viewUser)));

    }

    @Override
    public ViewUser updateUser(Integer userId, ViewUser viewUser) {
        if (!viewUser.getId().equals(userId)) {
            throw new InvalidParameterException(
                    "Provided user id: " + userId.toString() +
                            "does not match user: " + viewUser
            );
        }
        DomainUser domainUser = userAccessor.findById(userId)
                .orElseThrow(() -> new EntityNotFoundException(
                        "Unable to retrieve user: " + userId.toString()
                ));
        if(!usernamesAreEquivalent(domainUser.getUsername(), viewUser.getUsername())
                && !userEngine.usernameIsUnique(viewUser.getUsername())) {
            throw new InvalidParameterException(
                    "Username '" + viewUser.getUsername() + "' already exists."
            );
        }
        return userConverter.domainToView(
                userAccessor.save(
                        userConverter.viewToDomain(viewUser)
                )
        );
    }

    @Override
    public ViewUser deleteUser(Integer userId) {
        DomainUser domainUser = userAccessor.findById(userId)
                .orElseThrow(() -> new EntityNotFoundException(
                        "Unable to retrieve user: " + userId.toString()
                ));
        userAccessor.deleteById(userId);
        return userConverter.domainToView(domainUser);
    }

    private boolean usernamesAreEquivalent (String some, String other) {
        return some.equalsIgnoreCase(other);
    }
}
