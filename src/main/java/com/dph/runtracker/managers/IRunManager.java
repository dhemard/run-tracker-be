package com.dph.runtracker.managers;

import com.dph.runtracker.views.ViewRun;

import java.util.List;

public interface IRunManager {
    List<ViewRun> getAllRuns(Integer userId);
    ViewRun getRunById(Integer userId, Integer runId);
    ViewRun createRun(Integer userId, ViewRun viewRun);
    ViewRun updateRun(Integer userId, Integer runId, ViewRun viewRun);
    ViewRun deleteRun(Integer userId, Integer runId);
}
