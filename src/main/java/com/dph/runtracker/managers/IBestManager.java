package com.dph.runtracker.managers;

import com.dph.runtracker.views.ViewBest;

public interface IBestManager {
    ViewBest getCurrentPersonalBest(Integer userId);
}
