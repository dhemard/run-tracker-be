CREATE TABLE run (
  id          INT AUTO_INCREMENT PRIMARY KEY,
  user_id     INT NOT NULL,
  start_time  TIMESTAMP NOT NULL,
  elapsed     FLOAT NOT NULL,
  distance    FLOAT NOT NULL,
  rating      INT,
  created_on  TIMESTAMP NOT NULL,
  modified_on TIMESTAMP NOT NULL,
  deleted     BOOLEAN DEFAULT FALSE,
  FOREIGN KEY (user_id) REFERENCES user(id)
);