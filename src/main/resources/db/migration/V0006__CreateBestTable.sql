CREATE TABLE best (
  id        INT AUTO_INCREMENT PRIMARY KEY,
  user_id   INT NOT NULL,
  distance  FLOAT,
  elapsed   FLOAT,
  avg_pace  FLOAT,
  FOREIGN KEY (user_id) REFERENCES user(id)
);