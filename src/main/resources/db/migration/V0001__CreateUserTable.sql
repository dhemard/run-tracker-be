CREATE TABLE user (
  id            INT AUTO_INCREMENT PRIMARY KEY,
  username      VARCHAR (255) NOT NULL,
  first_name    VARCHAR (255) DEFAULT '',
  last_name     VARCHAR (255) DEFAULT '',
  gender        VARCHAR (6),
  height        FLOAT,
  weight        FLOAT,
  dob           DATE,
  created_on    TIMESTAMP NOT NULL,
  modified_on   TIMESTAMP NOT NULL,
  deleted       BOOLEAN DEFAULT FALSE NOT NULL
)