INSERT INTO user (username, first_name, last_name, gender, height, weight, dob, created_on, modified_on, deleted) VALUES
  ('bwilliams', 'Bill', 'Williams', 'male', 60, 150, '1995-08-02', '2017-02-23 13:01:01', '2017-02-23 13:01:01', FALSE),
  ('mwhite', 'Megan', 'White', 'female', 63.5, 152.5, '1974-12-10', '2017-02-23 13:01:01', '2017-02-23 13:01:01', FALSE),
  ('eblue', 'Ellen', 'Blue', 'female', 55, 121.5, '1990-02-14', '2017-02-23 13:01:01', '2017-02-23 13:01:01', FALSE),
  ('jgreenwood', 'Johnny', 'Greenwood', 'female', 0 , 0, '1971-11-05', '2017-02-23 13:01:01', '2017-02-23 13:01:01', TRUE),
  ('jgreenwood', 'Johnny', 'Greenwood', 'male', 72, 160, '1971-11-05', '2017-02-24 13:00:00', '2017-02-24 13:00:00', FALSE);